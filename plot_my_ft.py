import matplotlib.pyplot as plt
import numpy as np
from time import time as time_now

from ft import ft
from read_and_plot import read_data

if __name__ == '__main__':
    time, signal1, signal2 = read_data('signal1.txt', step=1)
    time1, signal11, signal22 = read_data('signal1.txt', step=4)
    time_start = time_now()
    xf, yf = ft(time1, signal11)
    print('Time elapsed: {}'.format(time_now() - time_start))

    fig1, ax1 = plt.subplots()
    fig2, ax2 = plt.subplots()
    ax1.plot(time, signal1)
    dt = time1[1] - time1[0]
    ax2.plot(xf, dt * np.abs(yf))
    plt.show()
