import matplotlib.pyplot as plt
import numpy as np
from scipy import fftpack

from filters import bandpass
from read_and_plot import read_data

if __name__ == '__main__':
    time, signal1, signal2 = read_data('signal1.txt', step=1)
    N = len(time)
    T = time[1]
    old_signal = signal1
    signal1 = bandpass(signal1, 0.1, 0.2, 10)
    y1 = fftpack.fft(old_signal)
    yf = fftpack.fft(signal1)

    fig1, ax1 = plt.subplots()
    ax1.set_title('Сигнал (полосный фильтр)')
    ax1.plot(time[:len(old_signal)], old_signal)
    ax1.plot(time[:len(signal1)], signal1)

    xf = np.linspace(0.0, 1.0 // (2.0 * T), N // 2)
    fig2, ax2 = plt.subplots()
    ax2.set_title('Фурье-образ (полосный фильтр)')
    ax2.plot(xf, T * np.abs(y1[:N // 2]))
    ax2.plot(xf, T * np.abs(yf[:N // 2]))
    plt.show()
