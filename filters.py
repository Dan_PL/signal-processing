from cmath import sin, pi, cos

ALPHA = 0.5


def moving_average(signal, taps=10):
    filtered_signal = signal[:taps]
    for i in range(taps, len(signal)):
        filtered_signal.append(0.0)
        for k in range(0, taps):
            filtered_signal[i] += signal[i-k] / taps
    return filtered_signal


def ideal_impulse_response(n, cutoff):
    if n == 0:
        return cutoff / pi
    return sin(cutoff * n) / pi / n


def hann_window(n, width):
    return 0.5 + 0.5 * cos(2 * pi * n / width)


def hamming_window(n, width):
    return 0.54 + 0.46 * cos(2 * pi * n / width)


def blackman_window(n, width):
    return 0.42 + 0.5 * cos(2 * pi * n / width) + 0.08 * cos(4 * pi * n / width)


def impulse_response(n, cutoff, width):
    return ideal_impulse_response(n, cutoff / 2 / pi) * blackman_window(n, width)


def lowpass(signal, cutoff, width):
    filtered_signal = [signal[0]]
    for i in range(1, len(signal)):
        filtered_signal.append(0.0)
        for k in range(0, width):
            filtered_signal[i] += (signal[i - k] * impulse_response(k, cutoff, width)) if (i - k) >= 0 else 0.0
    return filtered_signal


def left_ideal_impulse(n, left):
    if n == 0:
        return -left / pi
    return -sin(left * n) / pi / n


def left_raised_cosine_window(n, width):
    return ALPHA + (1 - ALPHA) * sin(2 * pi * n / width)


def right_ideal_impulse(n, right):
    if n == 0:
        return right / pi
    return sin(right * n) / pi / n


def right_raised_cosine_window(n, width):
    return ALPHA + (1 - ALPHA) * cos(2 * pi * n / width)


def band_impulse_response(n, left, right, width):
    ideal_impulse = left_ideal_impulse(n, left / 2 / pi) + right_ideal_impulse(n, right / 2 / pi)
    window = left_raised_cosine_window(n, width) + right_raised_cosine_window(n, width)
    return ideal_impulse * window


def bandpass(signal, left, right, width):
    filtered_signal = [signal[0]]
    for i in range(1, len(signal)):
        filtered_signal.append(0.0)
        for k in range(0, width):
            filtered_signal[i] += (signal[i - k] * band_impulse_response(k, left, right, width)) if (i - k) >= 0 else 0.0
    return filtered_signal
