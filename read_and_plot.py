import matplotlib.pyplot as plt


def read_data(filename, step=1):
    with open(filename, 'r') as file:
        lines = [list(map(float, line.split())) for line in file.readlines()]
    time = []
    signal1 = []
    signal2 = []
    for t, s1, s2 in lines[::step]:
        time.append(t)
        signal1.append(s1)
        signal2.append(s2)
    return time, signal1, signal2


def main():
    time, signal1, signal2 = read_data('signal1.txt')
    plt.plot(time, signal1)
    plt.show()


if __name__ == '__main__':
    main()
