import matplotlib.pyplot as plt
import numpy as np
import scipy.fftpack
from time import time as time_now

from read_and_plot import read_data

if __name__ == '__main__':
    time, signal1, signal2 = read_data('signal1.txt', step=4)
    N = len(time)
    T = time[1]
    x = np.linspace(0.0, N * T, N)
    time_start1 = time_now()
    yf = scipy.fftpack.fft(signal1)
    time_start2 = time_now()
    iyf = scipy.fftpack.ifft(yf)
    print('Time ifft: {}'.format(time_now() - time_start2))
    print('Time both: {}'.format(time_now() - time_start1))

    fig1, ax1 = plt.subplots()
    ax1.plot(x, signal1)
    ax1.set_title('Сигнал')
    fig2, ax2 = plt.subplots()
    ax2.plot(x, iyf, color='orange')
    ax2.set_title('Восстановленный сигнал')
    plt.show()
