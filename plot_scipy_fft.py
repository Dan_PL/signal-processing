import matplotlib.pyplot as plt
import numpy as np
from scipy import fftpack
from time import time as time_now

from read_and_plot import read_data

if __name__ == '__main__':
    time, signal1, signal2 = read_data('signal1.txt', step=1)
    N = len(time)
    T = time[1]
    time_start = time_now()
    # signal1.extend([0.0 for _ in range(len(signal1))])
    yf = fftpack.fft(signal1)
    print('Time elapsed: {}'.format(time_now() - time_start))

    n = N // 2
    # n = N

    fig1, ax1 = plt.subplots()
    ax1.set_title('Сигнал')
    ax1.plot(time, signal1)
    fig2, ax2 = plt.subplots()

    xf = np.linspace(0.0, 1.0 // (2.0 * T), n)
    ax2.set_title('Фурье-образ')
    ax2.plot(xf, T * np.abs(yf[:n]))
    plt.show()
