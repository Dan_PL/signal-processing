from cmath import exp, pi

import numpy as np


def fourier_image(frequency, time, signal):
    s = 0
    for ti, yi in zip(time, signal):
        s += yi * exp(-2j * pi * frequency * ti)
    return s


def ft(time, signal):
    n = len(time)
    frequencies = np.linspace(0.0, 1.0 // (2.0 * time[1]), n // 2)
    return frequencies, [fourier_image(frequency, time, signal) for frequency in frequencies]


def fourier_inverse(moment, frequencies, amplitudes):
    s = complex()
    for qi, yi in zip(frequencies, amplitudes):
        s += yi * exp(2j * pi * moment * qi)
    return s.real / pi


def ift(frequencies, amplitudes):
    n = len(frequencies)
    time = np.linspace(0.0, 500.0, n)
    return time, [fourier_inverse(moment, frequencies, amplitudes) for moment in time]
