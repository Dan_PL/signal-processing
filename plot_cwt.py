from cmath import sin, exp, sqrt, cos
from time import time as time_now

import matplotlib.pyplot as plt
import numpy as np
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
from scipy import signal
from scipy.constants import pi

from read_and_plot import read_data


def v(x):
    if x < 0:
        return 0
    elif 0 <= x <= 1:
        return 10 * x ** 3 - 15 * x ** 4 + 6 * x ** 5
    else:
        return 1


# def v(x):
#     if x < 0:
#         return 0
#     elif 0 <= x <= 1:
#         return x
#     else:
#         return 1


def meyer_wavelet(w):
    if 2 * pi / 3 < abs(w) < 4 * pi / 3:
        return sin(v(3 * abs(w) / 2 / pi - 1) * pi / 2) * exp(w * 1j / 2) / sqrt(2 * pi)
    elif 4 * pi / 3 < abs(w) < 8 * pi / 3:
        return cos(v(3 * abs(w) / 4 / pi - 1) * pi / 2) * exp(w * 1j / 2) / sqrt(2 * pi)
    else:
        return 0.0


def meyer(size, width, s=1.0):
    return [meyer_wavelet(w) for w in np.linspace(-s * 2 * np.pi, s * 2 * np.pi, size)]


if __name__ == '__main__':
    time, signal1, signal2 = read_data('signal1.txt', step=32)
    time_start = time_now()
    frequencies = np.linspace(4.0, 8.0, 40)

    widths = [1 / frequency for frequency in frequencies[::-1]]

    mexican_hat = signal.ricker
    morlet = signal.morlet

    cwt_matr = signal.cwt(signal1, mexican_hat, widths)
    print('Time elapsed: {}'.format(time_now() - time_start))

    fig = plt.figure('Вейвлет-спектр')
    ax = Axes3D(fig)
    x, y = np.meshgrid(time, frequencies)
    ax.plot_surface(x, y, np.abs(cwt_matr), rstride=1, cstride=1, cmap=cm.jet)
    plt.show()
