import matplotlib.pyplot as plt
from time import time as time_now

from ft import ft, ift
from read_and_plot import read_data

if __name__ == '__main__':
    time, signal1, signal2 = read_data('signal1.txt', step=4)
    time1, signal11, signal22 = read_data('signal1.txt', step=4)
    time_start1 = time_now()
    xf, yf = ft(time1, signal11)
    time_start2 = time_now()
    inverted_time, inverted_signal = ift(xf, yf)
    print('Time ift: {}'.format(time_now() - time_start2))
    print('Time both: {}'.format(time_now() - time_start1))

    fig1, ax1 = plt.subplots()
    ax1.plot(time, signal1)
    dxf = xf[1] - xf[0]
    ax1.plot(inverted_time, [dxf * s for s in inverted_signal])
    plt.show()
