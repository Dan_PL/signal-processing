import matplotlib.pyplot as plt
import numpy as np
from scipy import fftpack

from filters import lowpass
from read_and_plot import read_data

if __name__ == '__main__':
    time, signal1, signal2 = read_data('signal1.txt', step=1)
    N = len(time)
    T = time[1]
    signal_before = signal1
    signal1 = lowpass(signal1, 0.05, 200)
    yf = fftpack.fft(signal1)
    yf1 = fftpack.fft(signal_before)

    fig1, ax1 = plt.subplots()
    ax1.set_title('Сигнал (фильтр низких частот)')
    ax1.plot(time[:len(signal_before)], signal_before)
    ax1.plot(time[:len(signal1)], signal1)

    xf = np.linspace(0.0, 1.0 // (2.0 * T), N // 2)
    fig2, ax2 = plt.subplots()
    ax2.set_title('Фурье-образ (фильтр низких частот)')
    ax2.plot(xf, T * np.abs(yf1[:N // 2]))
    ax2.plot(xf, T * np.abs(yf[:N // 2]))
    plt.show()
