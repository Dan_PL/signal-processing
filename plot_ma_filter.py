import matplotlib.pyplot as plt
import numpy as np
from scipy import fftpack

from filters import moving_average
from read_and_plot import read_data

if __name__ == '__main__':
    time, signal1, signal2 = read_data('signal1.txt', step=1)
    N = len(time)
    T = time[1]
    yf1 = fftpack.fft(signal1)
    signal_before = signal1
    signal1 = moving_average(signal1, taps=50)
    yf = fftpack.fft(signal1)

    fig1, ax1 = plt.subplots()
    ax1.set_title('Сигнал (moving average filter)')
    ax1.plot(time, signal_before)
    ax1.plot(time, signal1)

    xf = np.linspace(0.0, 1.0 // (2.0 * T), N // 2)
    fig2, ax2 = plt.subplots()
    ax2.set_title('Фурье-образ (moving average filter)')
    ax2.plot(xf, T * np.abs(yf1[:N // 2]))
    ax2.plot(xf, T * np.abs(yf[:N // 2]))
    plt.show()
